/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qrcodeapplication;

import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

/**
 *
 * @author kopko
 */
public class QRcodeApplication extends Application {

    public static Config mainConfig;
    
    @Override
    public void start(Stage primaryStage) throws IOException {
    new Config().createConfig();
     FXMLLoader root =new FXMLLoader(getClass().getResource("mainSceneFXML.fxml"));
        Scene scene = new Scene(root.load());

        primaryStage.setScene(scene);

        primaryStage.setTitle("Hello JavaFX");
        primaryStage.setWidth(1380);
        primaryStage.setHeight(900);
        
        primaryStage.show();
        // init the controller
        MainSceneFXMLController controller = root.getController();
        controller.init();

        // set the proper behavior on closing the application
        primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
              
        if( TerminalModeFXMLController.camera!=null){
        TerminalModeFXMLController.camera.setClosed();}
            }
        }));
    
    }
    
    class MainPane extends BorderPane{
        
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
    }
    
}
