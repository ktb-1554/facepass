/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qrcodeapplication;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import qrcodeapplication.Config.ClothesHanger;
import qrcodeapplication.Config.Id;

/**
 * FXML Controller class
 *
 * @author kopko
 */
 public class TerminalModeFXMLController implements Initializable {

    private static ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    static VideoCapture capture;

    public static Camera camera;
    @FXML
    Separator separator = new Separator(Orientation.VERTICAL);
    @FXML
    ImageView cameraImage = new ImageView();
    @FXML
    GridPane queueListPane= new GridPane();
    @FXML
    Label idLabel= new Label();
    @FXML
    Button confirmationButton= new Button();
    static Runnable grabber;
    private static int cameraId = 0;
    static List<Queue> queueList= new ArrayList();
    
    public void initialize(URL url, ResourceBundle rb) {
//
//        String opencvpath = System.getProperty("user.dir") + "\\files\\";
//        String libPath = System.getProperty("java.library.path");
//        System.load(opencvpath + Core.NATIVE_LIBRARY_NAME + ".dll");
        camera = new Camera(cameraImage,queueListPane, idLabel);
        idLabel.setText("");
        
        confirmationButton.setOnMousePressed((event) -> {
            queueList.clear();
            queueListPane.getChildren().clear();
            QRcodeApplication.mainConfig.writeConfig();
        });
    }

    static class Camera {

        Camera(ImageView image, GridPane gridPane, Label label) {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            capture = new VideoCapture();
            capture.open(cameraId);
            System.out.println(capture.isOpened());
            // is the video stream available?
            if (capture.isOpened()) {
                grabber = new Runnable() {
                    public void run() {

                        // effectively grab and process a single frame
                        Mat frame = grabFrame();
                        // convert and show the frame

                        Image imageToShow = Utils.mat2Image(frame);

                        updateImageView(image, imageToShow);

                        String qrCode = decodeQRCode(Utils.matToBufferedImage(frame));
                        
                        if (qrCode != null && !qrCode.equals("")) {
                            ClothesHanger hanger=null;
                            Id id= QRcodeApplication.mainConfig.getIdFromString(qrCode);
                            System.out.println(existsInQueue(qrCode)+" exists in queue"); 
                            if(!existsInQueue(qrCode)&& queueList.size()<4){
                            if (QRcodeApplication.mainConfig.idExistsInConfig(qrCode)) {
                                
                                
                                hanger = QRcodeApplication.mainConfig.findClothesById(qrCode);
                                id= QRcodeApplication.mainConfig.getIdFromString(qrCode);
                                
                                QRcodeApplication.mainConfig.returnClothesToPerson(id, hanger);
                            queueList.add(new Queue(hanger.getNumber(),"return",qrCode));
                            } else {
                                
                                hanger = QRcodeApplication.mainConfig.getClothestEmptyHanger();
                              ///  System.out.println(hanger.getNumber()+"after");
                                QRcodeApplication.mainConfig.putClothesOnHanger(qrCode, hanger);
                                queueList.add(new Queue(hanger.getNumber(),"putOnH",qrCode));
                                            
                                }
                            
                            Platform.runLater(() -> {
                                
                            label.setText("Id:"+qrCode);
                            redrawQueuePane(gridPane);
                            });
                            pauseAcquisition(image);
                            
                            }
                        }
                    }
                };
                grabber.run();

                timer = Executors.newSingleThreadScheduledExecutor();
                timer.scheduleAtFixedRate(grabber, 0, 33, TimeUnit.MILLISECONDS);

                // grab a frame every 33 ms (30 frames/sec)
            } else {
                // log the error
                System.err.println("Impossible to open the camera connection...");
            }
        }

        private static Mat grabFrame() {
            // init everything
            Mat frame = new Mat();

            // check if the capture is open
            try {
                capture.read(frame);

            } catch (Exception e) {
                // log the error
                System.err.println("Exception during the image elaboration: " + e);
            }
            return frame;
        }

        /**
         * Stop the acquisition from the camera and release all the resources
         */
        private static void stopAcquisition() {
            if (timer != null && !timer.isShutdown()) {
                try {
                    // stop the timer
                    timer.shutdown();
                    timer.awaitTermination(33, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    // log any exception
                    System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
                }
            }

            if (capture.isOpened()) {
                // release the camera
                capture.release();
            }
        }

        private static void pauseAcquisition(ImageView image) {
            if (timer != null && !timer.isShutdown()) {
                try {
                    
                    timer.awaitTermination(1, TimeUnit.SECONDS);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TerminalModeFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

        /**
         * Update the {@link ImageView} in the JavaFX main thread
         *
         * @param view the {@link ImageView} to update
         * @param image the {@link Image} to show
         */
        private void updateImageView(ImageView view, Image image) {
            Utils.onFXThread(view.imageProperty(), image);
        }

        /**
         * On application close, stop the acquisition from the camera
         */
        public void setClosed() {
            this.stopAcquisition();
        }

    }
    static void setQueueToPane(Queue q,int index,GridPane queueListPane){
        
    queueListPane.add(q.numberLabel,0 , index);
    queueListPane.add(q.actionLabel, 1, index);
    System.out.println(index+" e");
    }
    public static void redrawQueuePane(GridPane queueListPane){
    queueListPane.getChildren().clear();
    for(int i=0;i<queueList.size();i++){
    setQueueToPane(queueList.get(i),i,queueListPane);
    System.out.println("Applying "+queueList.get(i).numberLabel.getText()+"-"+queueList.get(i).action);
    }
    
    }
    static class Queue{
        int hangerNumber;
        String id;
        String action;
        Label actionLabel= new Label();
        Label numberLabel= new Label();
            public Queue(int hangerNumber, String action, String id) {
            this.hangerNumber=hangerNumber;
            this.action=action;
            this.id= id;
            numberLabel.setText(hangerNumber+"");
            
            switch(this.action){
                case "putOnH": {
                    actionLabel.setText("Повесить одежду");
                    break;
                }
                case "return": {
                    
                    actionLabel.setText("Вернуть одежду");
                    break;
                }
            }
                actionLabel.setStyle("-fx-font-size: 48px;");
                numberLabel.setStyle("-fx-font-size: 96px;");
            }
            
    }
    
    
    static boolean existsInQueue(String id) {
        
        if (queueList.stream().anyMatch((q) -> (q.id != null && q.id.equals(id)))) {
            System.out.println(id);
            return true;
        }
        return false;
    }
    public void getQRcode() {
    }

    private static String decodeQRCode(BufferedImage qrCodeimage) {

        LuminanceSource source = new BufferedImageLuminanceSource(qrCodeimage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {

            Result result = new MultiFormatReader().decode(bitmap);
            
           System.out.println("There is QR code in the image- "+result.getText());
            return result.getText();

        } catch (NotFoundException e) {
            //System.out.println("There is no QR code in the image");
            return null;
        }
    }

}
