/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qrcodeapplication;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author kopko
 */
public class MainSceneFXMLController {

    @FXML
    ToggleButton terminalButton= new ToggleButton();
    @FXML
    ToggleButton settingsButton= new ToggleButton();
    
    @FXML
    HBox mainPane= new HBox();
    @FXML 
    VBox vbox= new VBox();
    @FXML
    HBox hbox= new HBox();
    @FXML
    ToggleGroup toggleBar= new ToggleGroup();

    @FXML
    private void setSettingsMode(ActionEvent event) throws IOException{
    mainPane.getChildren().clear();
        
        Node n=FXMLLoader.load(getClass().getResource("settingsMode.fxml"));
        
        mainPane.setHgrow(n, Priority.ALWAYS);
        mainPane.getChildren().add(n);
    
    }
    
    @FXML
    private void setTerminalMode(ActionEvent event) throws IOException{
        mainPane.getChildren().clear();
  
        Node n=FXMLLoader.load(getClass().getResource("terminalMode.fxml"));
        
        mainPane.setHgrow(n, Priority.ALWAYS);
        mainPane.getChildren().add(n);
    
    }
    
    void init() {
        mainPane.setFillHeight(true);
        terminalButton.setToggleGroup(toggleBar);
        settingsButton.setToggleGroup(toggleBar);
    } 
	



}
