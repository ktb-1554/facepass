/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qrcodeapplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kopko
 */
public class Config implements Serializable{

    int firstNumber;
    int lastNumber;
    int rowNumbers;
    byte section;
    int hangersInRowNumber;
    float closestRow;
    ClothesHanger[] hangers = new ClothesHanger[lastNumber - firstNumber + 1];
    String directoryPath;
    String databaseConnectionName;
    List<Id> idList= new ArrayList();
    public static class Id implements Serializable{
        String  id;
        public Id(String id) {
            this.id=id;
        }
    }

    public static class ClothesHanger implements Serializable {

        int number;
        boolean isEmpty = false;
        double pathNumber;
        Id id;

        ClothesHanger(int number, boolean isEmpty) {
            this.number = number;
            this.isEmpty = isEmpty;
        }

        void setId(Id i){
        this.id=i;}
        void clearId(){
        this.id= null;
        }
        void clearClothes() {
            this.isEmpty = true;
        }

        void setClothes() {
            this.isEmpty = false;
        }

        boolean isEmpty() {
            return isEmpty;
        }

        void setNumber(int n) {
            this.number = n;
        }

        int getNumber() {
            return number;
        }

    }

    Config() {
        this.setDefaultSettings(); //TODO: нужно для дебага, убрать позже
        this.fillConfig();
    }
    private void setDefaultSettings(){
    this.firstNumber=1;
    this.lastNumber=200;
    this.rowNumbers=2;
    this.closestRow=(float) 1.5;
    this.hangersInRowNumber=100;
    
    
    }
    public void fillConfig() {
        
        if (lastNumber > firstNumber && lastNumber > 0 && rowNumbers > 0 //Проверка на совпадение главных параметров гардероба
                && rowNumbers < lastNumber - firstNumber
               
                && closestRow < rowNumbers) {
            
            hangers = new ClothesHanger[lastNumber - firstNumber + 1];
            int arrayNumber = 0;
            for (int i = firstNumber; i <= lastNumber; i++) {

                hangers[arrayNumber] = new ClothesHanger(i, true);
                hangers[arrayNumber].pathNumber= Math.abs(closestRow - (i / (hangersInRowNumber / 2 + 1) + 1)) + (i - 1) % (hangersInRowNumber / 2);
                
                System.out.println(hangers[arrayNumber].pathNumber);
                arrayNumber++;
            }
            
                System.out.println(hangers.length);
        }
    }
    public ClothesHanger getClothestEmptyHanger(){
        ClothesHanger numberId = null;
        double path=100000000000.0; //ЧИсло, большее любого числа путя вешалок
        for(ClothesHanger h: hangers){
            
            if(h.isEmpty()){
                if(h.pathNumber<=path){
                    System.out.println(h.pathNumber);
                    path=h.pathNumber;
                    numberId=h;
                }
            }
        }   
    System.out.println(numberId.getNumber());
    return numberId;
    }
    
    public boolean idExistsInConfig(String personId){
    
        if (idList.stream().anyMatch((i) -> (personId.equals(i.id)))) return true;
            
        return false;
    }
    
    
    public void putClothesOnHanger(String personId,ClothesHanger hanger){
    if(!idExistsInConfig(personId)){
     Id perId= new Id(personId);//регистрация id
     idList.add(perId);
     hanger.setClothes();
     hanger.setId(perId);
     
    }
    }
      
    public ClothesHanger findClothesById(String personId){
    if(idExistsInConfig(personId)){
    for(ClothesHanger hanger:hangers){
     if(hanger.id!=null && hanger.id.id.equals(personId) ){
         
     System.out.println(hanger.id.id);
     return hanger;
     }
    } 
    
    }
    return null;
    }
    public Id getIdFromString(String id){
        for(Id i:idList){
        if(i.id.equals(id)){return i;}
        }
        return null;
    }

    public void returnClothesToPerson(Id personId, ClothesHanger hanger) {
        if (idExistsInConfig(personId.id)) {

            idList.remove(personId);//регистрация id
            hanger.clearClothes();
            hanger.clearId();

        }
    }

    public void readConfig(String path) {
        QRcodeApplication.mainConfig = new Config();

        FileInputStream fis;
        try {
            fis = new FileInputStream(path);
            ObjectInputStream oin = new ObjectInputStream(fis);
            try {
                QRcodeApplication.mainConfig = (Config) oin.readObject();
                System.out.println("Конфиг успешно считался.");
            } catch (ClassNotFoundException ex) {
                System.out.println("Конфиг не считался.");
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Конфиг не считался.");
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("Конфиг не считался.");
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void writeConfig() {

            File f = new File(getJarAdress()+"/config.ini");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            }
            try (ObjectOutputStream oos = new ObjectOutputStream(fos)) {

                oos.writeObject(this);
                oos.flush();
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void createConfig() {
        if (new File(getJarAdress() + "/config.ini").exists()) {
            System.out.println("Конфиг уже существует.");
            readConfig(getJarAdress() + "/config.ini");
        } else {
            try {
                new File(getJarAdress() + "/config.ini").createNewFile();
                QRcodeApplication.mainConfig = new Config();
                QRcodeApplication.mainConfig.writeConfig();

                System.out.println("Конфиг создан по адресу "+getJarAdress()+"\\config.ini .");
            } catch (IOException ex) {
                System.out.println("Ошибка создания конфига.");
            }

        }

    }

    public static String getJarAdress() {
        File file = new File(System.getProperty("java.class.path"));
        String path = file.getParent();
        // System.out.println("Путь до jar файла - "+path);
        return path;
        
    }
    
}
