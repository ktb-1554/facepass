/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qrcodeapplication;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author kopko
 */
public class SettingsFXMLController {

    int numberOfHangersInRow = QRcodeApplication.mainConfig.hangersInRowNumber;
    String rangeString = QRcodeApplication.mainConfig.firstNumber + "-" + QRcodeApplication.mainConfig.lastNumber;
    float closestRow = QRcodeApplication.mainConfig.closestRow;
    int numberOfRows = QRcodeApplication.mainConfig.rowNumbers;

    @FXML
    Button confirmationButton = new Button();
    @FXML
    TextField rowNumberField = new TextField();
    @FXML
    TextField numberOfHangersInRowField = new TextField();
    @FXML
    TextField placeOfTerminalField = new TextField();
    @FXML
    TextField numberRangeField = new TextField();
    
    @FXML
    void initialize() {
        if (TerminalModeFXMLController.camera != null) {
            TerminalModeFXMLController.camera.setClosed();
        }
        setInformationInTextFields();
        confirmationButton.setOnMousePressed((event) -> {
           generateConfigDatabase();
        });

    }
    public void generateConfigDatabase(){
    
     try {
                int firstNumber = Integer.parseInt(numberRangeField.getText().split("-")[0]);
                int lastNumber = Integer.parseInt(numberRangeField.getText().split("-")[1]);
                if (lastNumber > firstNumber) {
                    QRcodeApplication.mainConfig.firstNumber = firstNumber;
                    QRcodeApplication.mainConfig.lastNumber = lastNumber;
                }
                if ((int) Float.parseFloat(placeOfTerminalField.getText()) <= Integer.parseInt(rowNumberField.getText())) {

                    QRcodeApplication.mainConfig.closestRow = Float.parseFloat(placeOfTerminalField.getText());
                }

                QRcodeApplication.mainConfig.hangersInRowNumber = Integer.parseInt(numberOfHangersInRowField.getText());

                QRcodeApplication.mainConfig.rowNumbers = Integer.parseInt(rowNumberField.getText());
                QRcodeApplication.mainConfig.writeConfig();
                QRcodeApplication.mainConfig.fillConfig();
            } catch (Exception e) {
                setInformationInTextFields();
            }
    
    }
    void setInformationInTextFields() {
        if (numberOfHangersInRow != 0) {
            numberOfHangersInRowField.setText("" + numberOfHangersInRow);
        }
        if (closestRow != 0) {
            placeOfTerminalField.setText("" + closestRow);
        }

        if (numberOfRows != 0) {
            rowNumberField.setText("" + numberOfRows);
        }

        if (!rangeString.equals("0-0")) {
            numberRangeField.setText(rangeString);
        }
    }
}
